package com.devcamp.j01_javabasic.s50;

public class Voucher {
    public String voucherCode = "V000000";

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public void showVoucher() {
        System.out.println("Voucher code is: " + this.voucherCode);
    }

    public void showVoucher(String voucherCode) {
        System.out.println("This is voucher code: " + this.voucherCode);
        System.out.println("This is voucher code too: " + voucherCode);
    }

    public static void main(String[] args) throws Exception {
        Voucher voucher = new Voucher();
        
        //0. Chạy class này rồi cmt dòng dưới
        //voucher.showVoucher();

        //1. Bỏ cmt dòng code dưới rồi chạy lại class này
        //voucher.showVoucher("CODE8888");

        /*
         * 2. Cmt lại 2 dòng code trên
         * Bỏ cmt 2 dòng code dưới rồi chạy lại class này
         */
        //voucher.voucherCode = "CODE2222";
        //voucher.showVoucher("0008888");

        /*
         * 3. Cmt lại 2 dòng code trên
         * Bỏ cmt các dòng code dưới đây rồi chạy lại class này
         */
        String code = "VOUCHER";
        voucher.setVoucherCode(code);
        System.out.println(voucher.getVoucherCode());
        voucher.showVoucher(voucher.getVoucherCode());
        voucher.showVoucher();
    }
}
